import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserserviceService } from 'src/services/userservice.service';

@Component({
  selector: 'app-viewrepos',
  templateUrl: './viewrepos.component.html',
  styleUrls: ['./viewrepos.component.css']
})
export class ViewreposComponent implements OnInit {
  public _login;
  public repos;

  constructor(private route: ActivatedRoute,private userserviceService: UserserviceService) {
    this.route.params.subscribe((params: any) => {
      console.log(params);
      this._login = (params.login !== undefined)? params.login : '';
    });
   }

  ngOnInit() {
    this.userserviceService.GetAllReposByPerson(this._login).subscribe(
      res=>{
        this.repos = res;
        console.log(res);
      }
      
    );
  }

}
