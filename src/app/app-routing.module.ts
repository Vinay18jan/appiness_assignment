import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './searchuser/search.component';
import { ViewreposComponent } from './viewrepos/viewrepos.component';


const routes: Routes = [
  {
    path:'',
    component: SearchComponent

  },
  {
    path:'view/:login',
    component: ViewreposComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
