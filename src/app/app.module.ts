import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './searchuser/search.component';
import { UserserviceService } from 'src/services/userservice.service';
import { HttpClientModule } from '@angular/common/http';
import { ViewreposComponent } from './viewrepos/viewrepos.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    ViewreposComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule 
  ],
  providers: [UserserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
