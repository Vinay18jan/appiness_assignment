import { Component, OnInit } from '@angular/core';
import { UserGetType } from 'src/types/usergettype';
import { UserserviceService } from 'src/services/userservice.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  public userList: UserGetType;

  constructor(private userserviceService: UserserviceService) { 
    this.userList = new UserGetType();
  }

  ngOnInit() {
    this.userserviceService.GetAllUsers(2020)
    .subscribe(
      res=>{
        this.userList = res;
      }
    );
  }
}
