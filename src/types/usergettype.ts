export class UserGetType {
    id: any;
    name: any;
    login: any;
    url: any;
    public_repos: any;
    //avatar_url: string

    constructor(){
        this.id = undefined;
        this.name = undefined;
        this.login = undefined;
        this.url = undefined;
        this.public_repos = undefined;
        //this.avatar_url = undefined;
    }
}