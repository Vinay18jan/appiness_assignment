import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {
  private _GetAllUsers = "https://api.github.com/users";
  public token;
  public userid;

  constructor(private _http: HttpClient) { }

  GetAllUsers(year) {
    return this._http.get<any>(this._GetAllUsers + "?since=" + year);
  }

  GetAllReposByPerson(login){
    return this._http.get<any>(this._GetAllUsers + "/" + login + "/repos");
  }
}
